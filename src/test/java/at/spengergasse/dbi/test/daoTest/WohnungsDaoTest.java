package at.spengergasse.dbi.test.daoTest;

import at.spengergasse.dbi.heu16959.dao.WohnungDao;
import at.spengergasse.dbi.heu16959.model.Wohnung;
import org.elasticsearch.action.delete.DeleteResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

public class WohnungsDaoTest {

    @Test
    public void testSaveAndFindWohnung(){
        Wohnung wohnung = new Wohnung();
        wohnung.setRentalPrice(970);
        wohnung.setRoomCount(4);
        wohnung.setSquareMeters(120);

        WohnungDao dao = new WohnungDao();
        List<Wohnung> resultList = null;
        try {
            dao.saveEntity(wohnung, "wohnung", "json");
            resultList = dao.findAllEntitiesWithIndex("wohnung");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assertions.assertNotNull(resultList);
    }

    //Dieser Test funktioniert aus unerklärlichen Gründen immer erst beim 2. Versuch, also bitte mindestens 2 mal probieren
    @Test
    public void testSaveAndDeleteWohnung(){
        Wohnung wohnung = new Wohnung();
        wohnung.setRentalPrice(970);
        wohnung.setRoomCount(4);
        wohnung.setSquareMeters(120);

        WohnungDao dao = new WohnungDao();
        List<Wohnung> resultList;
        DeleteResponse deleteResponse = null;
        try {
            dao.saveEntity(wohnung, "wohnung", "json");
            resultList = dao.findAllEntitiesWithIndex("wohnung");
            deleteResponse = dao.deleteEntity(wohnung, "wohnung", "json", resultList.iterator().next().getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assertions.assertTrue(deleteResponse.status().getStatus() == 200);
    }
}
