package at.spengergasse.dbi.test.daoTest;

import at.spengergasse.dbi.heu16959.dao.MieterDao;
import at.spengergasse.dbi.heu16959.dao.VermieterDao;
import at.spengergasse.dbi.heu16959.model.Mieter;
import at.spengergasse.dbi.heu16959.model.Vermieter;
import org.elasticsearch.action.delete.DeleteResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class MieterDaoTest {
    @Test
    public void testSaveAndFindVermieter(){
        Mieter mieter = new Mieter();
        mieter.setDateOfBirth(LocalDate.of(1995,8,23));
        mieter.setFirstName("Max");
        mieter.setLastName("Mustermann");

        MieterDao dao = new MieterDao();
        List<Mieter> resultList = null;
        try {
            dao.saveEntity(mieter, "mieter", "json");
            resultList = dao.findAllEntitiesWithIndex("mieter");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assertions.assertNotNull(resultList);
    }

    //Dieser Test funktioniert aus unerklärlichen Gründen immer erst beim 2. Versuch, also bitte mindestens 2 mal probieren
    @Test
    public void testSaveAndDeleteVermieter(){
        Mieter mieter = new Mieter();
        mieter.setDateOfBirth(LocalDate.of(1995,8,23));
        mieter.setFirstName("Max");
        mieter.setLastName("Mustermann");

        MieterDao dao = new MieterDao();
        List<Mieter> resultList = null;
        DeleteResponse deleteResponse = null;
        try {
            dao.saveEntity(mieter, "mieter", "json");
            resultList = dao.findAllEntitiesWithIndex("mieter");
            deleteResponse = dao.deleteEntity(mieter, "mieter", "json", resultList.iterator().next().getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assertions.assertTrue(deleteResponse.status().getStatus() == 200);
    }
}
