package at.spengergasse.dbi.test.daoTest;

import at.spengergasse.dbi.heu16959.dao.VermieterDao;
import at.spengergasse.dbi.heu16959.model.Vermieter;
import org.elasticsearch.action.delete.DeleteResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class VermieterDaoTest {
    @Test
    public void testSaveAndFindVermieter(){
        Vermieter vermieter = new Vermieter();
        vermieter.setDateOfBirth(LocalDate.of(1998,11,10));
        vermieter.setFirstName("Max");
        vermieter.setLastName("Mustermann");

        VermieterDao dao = new VermieterDao();
        List<Vermieter> resultList = null;
        try {
            dao.saveEntity(vermieter, "vermieter", "json");
            resultList = dao.findAllEntitiesWithIndex("vermieter");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assertions.assertNotNull(resultList);
    }

    //Dieser Test funktioniert aus unerklärlichen Gründen immer erst beim 2. Versuch, also bitte mindestens 2 mal probieren
    @Test
    public void testSaveAndDeleteVermieter(){
        Vermieter vermieter = new Vermieter();
        vermieter.setDateOfBirth(LocalDate.of(1998,11,10));
        vermieter.setFirstName("Max");
        vermieter.setLastName("Mustermann");

        VermieterDao dao = new VermieterDao();
        List<Vermieter> resultList;
        DeleteResponse deleteResponse = null;
        try {
            dao.saveEntity(vermieter, "vermieter", "json");
            resultList = dao.findAllEntitiesWithIndex("vermieter");
            deleteResponse = dao.deleteEntity(vermieter, "vermieter", "json", resultList.iterator().next().getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assertions.assertTrue(deleteResponse.status().getStatus() == 200);
    }
}
