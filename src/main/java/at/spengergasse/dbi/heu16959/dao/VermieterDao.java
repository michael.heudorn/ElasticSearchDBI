package at.spengergasse.dbi.heu16959.dao;

import at.spengergasse.dbi.heu16959.model.Vermieter;

public class VermieterDao extends AbstractDao<Vermieter> {
    public VermieterDao(){
        super(Vermieter.class);
    }
}
