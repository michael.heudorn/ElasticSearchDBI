package at.spengergasse.dbi.heu16959.dao;

import at.spengergasse.dbi.heu16959.model.Mieter;

public class MieterDao extends AbstractDao<Mieter>{
    public MieterDao(){
        super(Mieter.class);
    }
}
