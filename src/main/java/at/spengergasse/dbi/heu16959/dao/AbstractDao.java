package at.spengergasse.dbi.heu16959.dao;

import at.spengergasse.dbi.heu16959.model.AbstractPersistable;
import com.google.gson.Gson;
import lombok.Getter;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDao<T extends AbstractPersistable> {

    @Getter
    private Class<T> persistentClass;

    @Getter
    private RestHighLevelClient restHighLevelClient;

    @Getter
    private Header header;

    @Getter
    private Gson gson;

    public AbstractDao(Class<T> entity){
        restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost",9200,"http")));
        header = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        gson = new Gson();
        persistentClass = entity;
    }

    public List<T> findAllEntitiesWithIndex(String index) throws IOException {
        if(index == null || index.length() == 0){
            throw new IllegalArgumentException("You have to provide an index!");
        }
        SearchRequest searchRequest = new SearchRequest(index);
        SearchResponse searchResponse = getRestHighLevelClient().search(searchRequest, getHeader());
        List<T> result = new ArrayList<>();
        for(SearchHit hit : searchResponse.getHits()){
            T entity = getGson().fromJson(hit.getSourceAsString(), persistentClass);
            entity.setId(hit.getId());
            result.add(entity);
        }
        return result;
    }

    public IndexResponse saveEntity(T entity, String index, String type) throws IOException{
        if(entity == null){
            throw new IllegalArgumentException("You have to provide an entity to save!");
        }
        IndexRequest request = new IndexRequest(index, type);
        request.source(getGson().toJson(entity), XContentType.JSON);
        return getRestHighLevelClient().index(request, getHeader());
    }

    public DeleteResponse deleteEntity(T entity, String index, String type, String id) throws IOException {
        if(entity == null){
            throw new IllegalArgumentException("You have to provide an entity to delete!");
        }
        DeleteRequest request = new DeleteRequest(index,type,id);
        return getRestHighLevelClient().delete(request, getHeader());
    }
}
