package at.spengergasse.dbi.heu16959.dao;

import at.spengergasse.dbi.heu16959.model.Wohnung;

public class WohnungDao extends AbstractDao<Wohnung>{
    public WohnungDao(){
        super(Wohnung.class);
    }
}
