package at.spengergasse.dbi.heu16959.model;

import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter @Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Mieter extends AbstractPersistable{
    private String firstName;
    private String lastName;
    private String eMail;
    private String phoneNumber;
    private LocalDate dateOfBirth;
    private List<Wohnung> wohnungen;

    public Mieter(){
        wohnungen = new ArrayList<Wohnung>();
    }

    public List<Wohnung> getWohnungen(){
        return Collections.unmodifiableList(wohnungen);
    }

    public boolean addToWohnungen(Wohnung wohnung){
        if(wohnung != null && !wohnungen.contains(wohnung)){
            wohnungen.add(wohnung);
            return true;
        }
        return false;
    }

    public boolean removeFromWohnungen(Wohnung wohnung){
        if(wohnung != null && wohnungen.contains(wohnung)){
            wohnungen.remove(wohnung);
            return true;
        }
        return false;
    }
}
