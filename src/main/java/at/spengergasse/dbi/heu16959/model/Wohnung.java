package at.spengergasse.dbi.heu16959.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@ToString
public class Wohnung extends AbstractPersistable{
    private double rentalPrice;
    private int squareMeters;
    private int roomCount;
}
