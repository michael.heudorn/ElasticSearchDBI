package at.spengergasse.dbi.heu16959.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class AbstractPersistable {
    private String id;
}
